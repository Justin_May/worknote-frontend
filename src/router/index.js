import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import CreateNote from '@/components/CreateNote'
import NoteList from '@/components/NoteList'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/create',
      name: 'CreateNote',
      component: CreateNote
    },
    {
      path: '/list',
      name:'NoteList',
      component:NoteList
    }
  ]
})
